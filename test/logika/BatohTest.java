package logika;



import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Testovacia trieda {@code BatohTest}.
 *
 * @author   «meno autora»
 * @version  «verzia alebo dátum»
 */
public class BatohTest
{
    /**
     * Predvolený konštruktor testovacej triedy BatohTest.
     */
    public BatohTest()
    {
    }


    /**
     * Vytvorí inštanciu triedy BatohTest so zadaným názvom.
     *
     * @param  názov  názov konštruovaného testu
     */
   


    /**
     * Inicializácia predchádzajúca spusteniu každého testu a pripravujúca
     * tzv. prípravok (fixture), čo je množina objektov, s ktorými budú
     * testy pracovať.
     */
    @Before
    protected void setUp()
    {
    }


    /**
     * „Upratovanie“ po teste – táto metóda je spustená po vykonaní každého
     * testu.
     */
    @After
    protected void tearDown()
    {
    }

    @Test
    public void test()
    {
    }
}

