package main;

import javafx.application.Application;
import javafx.stage.Stage;
import logika.Hra;
import logika.IHra;
import uiText.TextoveRozhrani;

public class AdventuraZaklad extends Application {

    public static void main(String[] args) {
        if (args.length == 0) {
            launch(args);
        } else {
            if (args[0].equals("-text")) {
                IHra hra = new Hra();
                TextoveRozhrani ui = new TextoveRozhrani(hra);
                ui.hraj();
                System.exit(0);
            } else {
                System.out.println("Neplatný parameter");
            }
        }
    }

    @Override
    public void start(Stage primaryStage) {

    }
}
