package logika;


/**
 * Trieda {@code PrikazKontrola} slúži na «doplňte opis»…
 *
 * @author   «Adrián Varga»
 * @version  «2020»
 */

public class PrikazKontrola implements IPrikaz  {
    
    private static final String NAZEV = "Kontrola";
    private HerniPlan herniPlan;
    private Hra hra;
    
    public PrikazKontrola(HerniPlan herniPlan, Hra hra) {
       this.herniPlan = herniPlan;
       this.hra = hra;
    }
@Override //Celý odstavec na určeni potreby a použitie príkazu kontrola - zisťuje priestor a obsah batohu pomocou contains
public String provedPrikaz(String... parametry) {
    Prostor aktualniProstor = herniPlan.getAktualniProstor();
    if (aktualniProstor.getNazev().equals("Metro")){
        if (herniPlan.getBatoh().obsahBatohu().contains("čierne_tričko")) {
            hra.setKonecHry(true);
            return "ČIERNA JE FARBA REVOLUCIONÁROV!! BERIEME VÁS DO STANICE NA DODATOČNÚ KONTROLU";
        }
    }
        
        else {
            return "Pokračujte v ceste domov!";}
    return "ok";        
           
   }
   

@Override
public String getNazev() {return NAZEV; }
}
