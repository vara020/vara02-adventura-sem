package logika;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;

/**
 * Trieda {@code PrikazPouzi} slúži na «doplňte opis»…
 *
 * @author   «Adrián Varga»
 * @version  «2020»
 */

/**Príkaz nefunkčný - mal použiť plynovú masku v priesto "Peško".
 * Ospravedlňujem sa.
   */
public class PrikazPouzi implements IPrikaz
{
    private static final String NAZEV = "pouzi";
    private HerniPlan herniplan;
    private Hra hra;
public PrikazPouzi(HerniPlan herniplan, Hra hra){
    this.herniplan = herniplan;
    this.hra = hra;}
    
    
    
    @Override
    public String provedPrikaz(String... parametry){
        if(parametry.length == 0){ //ošetrenie nezadania parametru použitého predmetu
            return "Čo by ste chceli použiť";
        }
        String nazovPouziteho = parametry[0];
        Batoh batoh = herniplan.getBatoh();
        if(batoh.obsahuje(nazovPouziteho)){
            Vec pouzitaVec = batoh.vratVec(nazovPouziteho);
            Collection<Prostor> okolie = herniplan.getAktualniProstor().getVychody();
            
            if(pouzitaVec.isOchrana()){
                for(Prostor susedny : okolie){
                    if(susedny.isPlyn()){
                        susedny.setSmola(true);
                    }
              return "Pouzili ste predmet "+nazovPouziteho+".";
            }
                
    }
} return "Bohužiaľ nejde";
}
@Override
    public String getNazev() {
        return NAZEV;
    }
}




