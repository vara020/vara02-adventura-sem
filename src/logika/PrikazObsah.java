package logika;


/**
 * Trieda {@code PrikazObsah} slúži na «doplňte opis»…
 *
 * @author   «Adrián Varga»
 * @version  «2020»
 */

public class PrikazObsah implements IPrikaz
{
    private static final String NAZEV = "batoh";
    private HerniPlan herniPlan;
    private Batoh batoh;
    //vypíše obsah batohu
    public PrikazObsah(HerniPlan herniplan) {
        this.herniPlan = herniplan;
        this.batoh = herniplan.getBatoh();
    }
    @Override
    public String provedPrikaz(String... parametry) {return batoh.obsahBatohu(); }
    
    @Override
    public String getNazev() { return NAZEV;}
}
