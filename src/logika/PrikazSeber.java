package logika;


/**
 * Trieda {@code PrikazSeber} slúži na «doplňte opis»…
 *
 * @author   «Adrián Varga»
 * @version  «2020»
 */
public class PrikazSeber implements IPrikaz
{
    private static final String NAZEV = "seber";
    private HerniPlan plan;
    private Batoh batoh;
    
    
    /**
    *  Konstruktor třídy
    *  
    *  @param plan herní plán, ve kterém se bude ve hře "chodit" 
    */    
    public PrikazSeber(HerniPlan plan) {
        this.plan = plan;
        this.batoh = plan.getBatoh();
    }

    /**
     *  Provádí příkaz "jdi". Zkouší se vyjít do zadaného prostoru. Pokud prostor
     *  existuje, vstoupí se do nového prostoru. Pokud zadaný sousední prostor
     *  (východ) není, vypíše se chybové hlášení.
     *
     *@param parametry - jako  parametr obsahuje jméno prostoru (východu),
     *                         do kterého se má jít.
     *@return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo, tak ....
            return "Čo mám zobrať?!? Zadaj názov veci, ktorú mám zobrať...";
        }

        String nazevSbiraneVeci = parametry[0];

        //Skúmam aktuálny priestor, aby som zistil aké veci sú tam..
        Prostor aktualniProstor = plan.getAktualniProstor();
        

        //Ošetruje nemožnosť zobrať si niečo, čo neexistuje
        if(aktualniProstor.obsahujeVec(nazevSbiraneVeci) == null){
            return "Toto tu nieje";
        }
            else{
            //odoberá a pridáva veci do batohu, rieši prenostieľnosť
            Vec sbiranaVec = aktualniProstor.odeberVec(nazevSbiraneVeci);
            if(sbiranaVec.isPrenostelnost()){
                if(batoh.vlozVec(sbiranaVec))
               return "vec " + nazevSbiraneVeci + " si vložil do batohu";
            }
            else{
            //vec nie je prenositeľná - return statement
            
            return "to neunesieš";
            }
         
    }
    return "ok";
}
    
    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
