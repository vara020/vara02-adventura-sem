package logika;


/**
 * Trieda {@code Batoh} slúži na «doplňte opis»…
 *
 * @author   «Adrián Varga»
 * @version  «2020»
 */
import java.util.*;


public class Batoh
{

    private Map<String, Vec> veci;
    
    public Batoh() { veci = new HashMap<>();}
    
    public boolean vlozVec (Vec vec) {
        if(vec.isPrenostelnost()){
        veci.put(vec.getNazev(), vec);
        return true;
    }
        else{
          return false;
        }
    
    }
    
    public String obsahBatohu(){
        String vratenytext = "V batohu sa nachádza:";
        for (String nazev : veci.keySet()){
            vratenytext += " " + nazev;
        }
        return vratenytext;
    }
    public boolean obsahuje(String nazovVeci){
        for(String kluc : veci.keySet()){
            if(kluc.equals(nazovVeci)){
                return true;
                
            }
        }
        return false;
    } 
        public Vec vratVec(String nazovVeci){
        Vec vratena = null;
        if(veci.containsKey(nazovVeci)){
            vratena = veci.get(nazovVeci);
        }
        return vratena;
    }
}

