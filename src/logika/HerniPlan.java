  package logika;


/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 *@version    pro školní rok 2016/2017
 */
public class HerniPlan {
    
    private Prostor aktualniProstor;
    private Prostor vyherniProstor;
    private Batoh batoh;
    
     /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        zalozProstoryHry();
        batoh = new Batoh();

    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory
        Prostor domov = new Prostor("Domov","Tu si môžeš vybrať veci, ktoré si chceš zo sebou zobrať a zároveň sa sem musíš večer vrátiť");
        Prostor praca = new Prostor("Práca", "Práca, v ktorej pracuješ. Okolo seba vidíš chaos a nepokoje - je na tebe, aby si sa im vyhol, môžeš si vybrať akým spôsobom sa skúsiš dopraviť domov");
        Prostor metro = new Prostor("Metro","Metro. Rozhodol si sa ísť metrom, metro je plné ľudí, čo ti poskytuje pocit bezpečia... Všetko sa však môže rýchlo zmeniť.");
        Prostor peso = new Prostor("Pešky","Na chodníku. Rozhodol si sa vyskúšať to po vlastných. Cesta je to ale najpomalšia a môže sa ti pri nej všeličo privodiť.");
        Prostor zastavka = new Prostor("Autobusová_zastávka","Autobus. Rozhodol si sa počkať na autobus... Len boh vie, čo sa ti môže pritrafiť zatiaľ, čo čakáš.");
        Prostor autobus = new Prostor("Autobus","Si na ceste domov autobusom");
        Prostor dom = new Prostor("Doma","Podarilo sa Vám vrátiť domov. Konečne v bezpečí!");
        
        // přiřazují se průchody mezi prostory (sousedící prostory)
        domov.setVychod(praca);
        praca.setVychod(metro);
        praca.setVychod(peso);
        praca.setVychod(zastavka);
        autobus.setVychod(dom);
        metro.setVychod(dom);
        autobus.setVychod(dom);
        peso.setVychod(dom);
        zastavka.setVychod(autobus);
        // nastavuje udalosť, ktorá sa má stať pri vstupe do autobusu
        autobus.setSmola(true);
        peso.setPlyn(true);
        // vytvára veci
        Vec btriko = new Vec("biele_tričko", true);
        Vec ctriko = new Vec("čierne_tričko", true);
        Vec obcianka = new Vec("občiansky_preukaz", true);
        Vec stolicka = new Vec("stolička", false);
        Vec maska = new Vec("plynová_maska", true);
        //pridáva ich do priestorov
        domov.vlozVec(btriko);
        domov.vlozVec(ctriko);
        domov.vlozVec(obcianka);
        domov.vlozVec(stolicka);
        domov.vlozVec(maska);
                
        aktualniProstor = domov; // hra začíná v domečku
        vyherniProstor = dom;       
    }
    
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */
    
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
       aktualniProstor = prostor;
    }
    
    public boolean jeVyhra(){
       return aktualniProstor.equals(vyherniProstor);
    }
    public Batoh getBatoh(){ return batoh; }

}
