package logika;

/**
 *  Třída PrikazJdi implementuje pro hru příkaz jdi.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 *@author     Jarmila Pavlickova, Luboš Pavlíček
 *@version    pro školní rok 2016/2017
 */
class PrikazJdi implements IPrikaz {
    private static final String NAZEV = "jdi";
    private HerniPlan plan;
    private Hra hra;
    
    /**
    *  Konstruktor třídy
    *  
    *  @param plan herní plán, ve kterém se bude ve hře "chodit" 
    */    
    public PrikazJdi(HerniPlan plan, Hra hra) {
        this.plan = plan;
        this.hra = hra;
    }

    /**
     *  Provádí příkaz "jdi". Zkouší se vyjít do zadaného prostoru. Pokud prostor
     *  existuje, vstoupí se do nového prostoru. Pokud zadaný sousední prostor
     *  (východ) není, vypíše se chybové hlášení.
     *
     *@param parametry - jako  parametr obsahuje jméno prostoru (východu),
     *                         do kterého se má jít.
     *@return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo (sousední prostor), tak ....
            return "Kde mám ísť? Zadaj názov priestoru";
        }
        Prostor aktualnaMiestnost = plan.getAktualniProstor(); //Nepustí to ďalej bez použitia príkazu "Kontrola"
        if (aktualnaMiestnost.getNazev().equals("Metro")) {
            return "Náhodná kontrol batohov - použi príkaz 'Kontrola'!!!";
        }    
        else{
        String smer = parametry[0];
        

        // zkoušíme přejít do sousedního prostoru
        Prostor sousedniProstor = plan.getAktualniProstor().vratSousedniProstor(smer);

        if (sousedniProstor == null) {
            return "Tam sa ísť nedá - pozri si plán!";
        }
        else {
            plan.setAktualniProstor(sousedniProstor); //Ukončenie hry výberom zlej cesty
            if(sousedniProstor.isSmola()){
                hra.setKonecHry(true);
                return "Počas cesty autobusom ste sa stali obeťou náhodnej kontroly,\n ktorá sa Vám po niekoľkých odchytených revolucionároch stala osudnou - idete do stanice na kontrolu..";
            
        }
        return sousedniProstor.dlouhyPopis();
    }
}
}
    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
