package logika;


/**
 * Trieda {@code vec} slúži na «doplňte opis»…
 *
 * @author   «meno autora»
 * @version  «verzia alebo dátum»
 */
public class Vec
{
    private String nazev;
    private boolean prenositelnost;
    private boolean ochrana = false;

    /**
     * Konštruktory objektov triedy vec.
     */
    public Vec(String nazev, boolean prenositelnost){
        this.nazev = nazev;
        this.prenositelnost = prenositelnost;
    }


    /**
     * Prázdna metóda – «tento komentár nahraďte vlastným opisom»…
     *
     * @param   y  opis významu parametra
     * @return     opis významu návratovej hodnoty – napr.: súčet x a y
     */
    public String getNazev(){
        return nazev;
    }
    /**Getter na ochranu danej veci.
       Plynová maska ochráni pred plynom */
    public boolean isOchrana(){
        return ochrana;
    }
    //opisuje prenositeľnosť predmetu
    public boolean isPrenostelnost(){
        return prenositelnost;
    }
}
