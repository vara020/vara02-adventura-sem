package uiText;

import java.io.*;
import java.util.Scanner;
import logika.IHra;

/**
 *  Class TextoveRozhrani
 * 
 *  Toto je uživatelského rozhraní aplikace Adventura
 *  Tato třída vytváří instanci třídy Hra, která představuje logiku aplikace.
 *  Čte vstup zadaný uživatelem a předává tento řetězec logice a vypisuje odpověď logiky na konzoli.
 *  
 *  
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 *@version    pro školní rok 2016/2017
 */

public class TextoveRozhrani {
    private IHra hra;

    /**
     *  Vytváří hru.
     */
    public TextoveRozhrani(IHra hra) {
        this.hra = hra;
    }

    /**
     *  Hlavní metoda hry. Vypíše úvodní text a pak opakuje čtení a zpracování
     *  příkazu od hráče do konce hry (dokud metoda konecHry() z logiky nevrátí
     *  hodnotu true). Nakonec vypíše text epilogu.
     */
    public void hraj() {
        System.out.println(hra.vratUvitani());

        // základní cyklus programu - opakovaně se čtou příkazy a poté
        // se provádějí do konce hry.

        while (!hra.konecHry()) {
            String radek = prectiString();
            System.out.println(hra.zpracujPrikaz(radek));
        }

        System.out.println(hra.vratEpilog());
    }

    public void hrajZeSouboru(File soubor) {
        try (BufferedReader ctecka = new BufferedReader (new FileReader(soubor))) //presunutie BufferedReader to parametrov try --> potreba ošetriť java.io.IOException;

        { //Len try ale nie je možné preložiť --> musím mať block catch 
            // pokiaľ mám len FileReader(bez try), nestarám sa o výnimku <FileNotFoundException>
            // BufferedReader --> obal pre FileReader

            System.out.println(hra.vratUvitani());
            // základní cyklus programu - opakovaně se čtou příkazy a poté
            // se provádějí do konce hry.
            // rovnaké ako prvý hraj constructor, ale --> načítanie zo súboru
            String radek = ctecka.readLine(); // Posun čteni pred cyklus while --> čítanie prvého riadku
            while (!hra.konecHry() && radek != null) {
                 System.out.println("***"+radek+"***");
                 System.out.println(hra.zpracujPrikaz(radek));
                 radek = ctecka.readLine(); //čítanie ostatných riadkov
            }

            System.out.println(hra.vratEpilog());
        }
        catch(FileNotFoundException e){ // Samostatná catch metóda nestačí, pretože síce výnimku chytí, ale nič nespraví :o
            System.out.println("soubor s príkazmi nenájdený"); // Ošetrenie výnimky a výpis textu

        }
        catch(IOException e){ // Ošetreni IOException
            System.out.println("Chyba pri praci so súborom");
        }

    }

    /**
     *  Metoda přečte příkaz z příkazového řádku
     *
     *@return    Vrací přečtený příkaz jako instanci třídy String
     */
    private String prectiString() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("> ");
        return scanner.nextLine();
    }

}
